export class NotificationService {
  constructor(Notification, $filter) {
    'ngInject';

    this._Notification = Notification;
    this._filter = $filter;
  }

  primary(message) {
    this._Notification.primary(message);
  }

  success(message) {
    this._Notification.success(message);
  }

  warning(message) {
    this._Notification.warning(message);
  }

  error(message) {
    this._Notification.error(message);
  }

  primaryKey(key) {
    this.primary(this._filter('translate')(key));
  }

  successKey(key) {
    this.success(this._filter('translate')(key));
  }

  warningKey(key) {
    this.warning(this._filter('translate')(key));
  }

  errorKey(key) {
    this.error(this._filter('translate')(key));
  }

  errorResponse(error) {
    if (error.data) {
      error.data.forEach(m => { this.error( this._filter('translate')(m.errorMessage)) });
    } else {
      this.errorKey('SERVER_ERROR');
    }
  }
}