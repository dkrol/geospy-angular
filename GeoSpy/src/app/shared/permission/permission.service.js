export class PermissionService {
  constructor(PermRoleStore, PermPermissionStore, PermissionConst, ConfigService) {
    'ngInject';

    this._permRoleStore = PermRoleStore;
    this._permPermissionStore = PermPermissionStore;
    this._permissionConst = PermissionConst;
    this._configService = ConfigService;
  }

  definePermission() {
    this._setPermission();
    this._setRole();
  }

  _setPermission() {
    // this._permPermissionStore.definePermission(this._permissionConst.PERMISSION_NAME.SEE_DASHBOARD, () => { return true; });
  }

  _setRole() {
    this._permRoleStore.defineRole(this._permissionConst.ROLE_NAME.AUTHORIZED, () => {
      return this._configService.getProfile() ? true : false;
    });

    this._permRoleStore.defineRole(this._permissionConst.ROLE_NAME.USER, () => {
      let token = this._configService.getJwtDecode();
      if (token) {
        return token.role.toUpperCase() === this._permissionConst.ROLE_NAME.USER;
      } else {
        return false;
      }
    });

    this._permRoleStore.defineRole(this._permissionConst.ROLE_NAME.ADMIN, () => {
      let token = this._configService.getJwtDecode();
      if (token) {
        return token.role.toUpperCase() === this._permissionConst.ROLE_NAME.ADMIN;
      } else {
        return false;
      }
    });
  }
}