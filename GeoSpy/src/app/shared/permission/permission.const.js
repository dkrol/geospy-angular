export const PermissionConst = {
  ROLE_NAME: {
    USER: 'USER',
    ADMIN: 'ADMIN',
    AUTHORIZED: 'AUTHORIZED'
  },
  PERMISSION_NAME: {
    SEE_DASHBOARD: 'SEE_DASHBOARD'
  }
};