export function LocalStorageService($window) {
  'ngInject';

  return {
    setItem: setItem,
    getItem: getItem
  };

  function setItem(name, data) {
    $window.localStorage.setItem(name, angular.toJson(data));
  }

  function getItem(name) {
    let result = angular.fromJson($window.localStorage.getItem(name));
    return result && result !== 'null' ? result : null;
  }
}