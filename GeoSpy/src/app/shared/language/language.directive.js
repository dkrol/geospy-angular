import { LanguageController } from './language.controller';

export function LanguageDirective() {
  'ngInject';

  return {
    templateUrl: 'app/shared/language/language.view.html',
    controller: LanguageController,
    controllerAs: 'langCtrl',
    restrict: 'E',
    scope: {}
  };
}