export function LanguageController(ConfigService, $translate) {
  'ngInject';
  let vm = this;
  vm.changeLang = changeLang;

  function changeLang(lang) {
    $translate.use(lang);
    ConfigService.setLanguage(lang);
  }
}


// export class LanguageController {
//   constructor(ConfigService, $translate) {
//     'ngInject';

//     this._ConfigService = ConfigService;
//     this._translate = $translate;
//   }

//   changeLang(lang) {
//     this._translate.use(lang);
//     this._ConfigService.setLanguage(lang);
//   }
// }