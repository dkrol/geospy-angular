export function TableService(NgTableParams) {
  'ngInject';

  class Table {
    constructor() {
      this._count = 15;
      this._counts = [15, 25, 50];
    }

    withDataset(dataset) {
      this._dataset = dataset;
      return this;
    }

    build() {
      let tableParams = this._getTableParams();
      let tableSettings = this._getTableSettings();

      return new NgTableParams(tableParams, tableSettings);
    }

    _getTableParams() {
      return {
        count: this._count
      };
    }

    _getTableSettings() {
      return {
        counts: this._counts,
        dataset: this._dataset
      };
    }
  }

  return {
    startBuild: () => new Table()
  };
}