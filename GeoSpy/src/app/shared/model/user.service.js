export function UserService() {

  class User {
    constructor() {
      'ngInject';

      this._userName = null;
      this._password = null;
      this._confirmPassword = null;
      this._token = null;
      this._isLogged = false;
    }

    get userName() {
      return this._userName;
    }

    set userName(value) {
      this._userName = value;
    }

    get password() {
      return this._password;
    }

    set password(value) {
      this._password = value;
    }

    get confirmPassword() {
      return this._confirmPassword;
    }

    set confirmPassword(value) {
      this._confirmPassword = value;
    }

    get token() {
      return this._token;
    }

    set token(value) {
      this._token = value;
    }

    get isLogged() {
      return this._isLogged;
    }

    set isLogged(value) {
      this._isLogged = value;
    }

    getSignin() {
      return {
        userName: this._userName,
        password: this._password
      }
    }

    getSigned() {
      return {
        userName: this._userName,
        token: this._token,
        isLogged: this._isLogged
      }
    }

    getSignup() {
      return {
        userName: this._userName,
        password: this._password,
        confirmPassword: this._confirmPassword
      }
    }
  }

  return {
    userBuild: () => new User()
  }
}