export function ParseHelperService() {
  'ngInject';

  return {
    getStringFromDate: getStringFromDate,
    getDateTimeFromString: getDateTimeFromString,
    getStringFromDateTime: getStringFromDateTime,
    getDateTimeStringDisplay: getDateTimeStringDisplay
  };

  function getDateTimeFromString(timestampLong) {
    let datetime = getObjectFromDatetimeLong(timestampLong);
    return new Date(datetime.year, (datetime.month - 1), datetime.day, datetime.hour, datetime.minute, datetime.second, 0);
  }

  function getStringFromDateTime(datetime) {
    let day = datetime.date.getDate();
    let month = datetime.date.getMonth() + 1;
    let year = datetime.date.getFullYear();
    let hour = datetime.time.getHours();
    let minute = datetime.time.getMinutes();
    let second = datetime.time.getSeconds();
    return `${setZero(year)}${setZero(month)}${setZero(day)}${setZero(hour)}${setZero(minute)}${setZero(second)}`;
  }

  function getStringFromDate(date) {
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    return `${setZero(year)}${setZero(month)}${setZero(day)}`;
  }

  function getDateTimeStringDisplay(datetimeLong) {
    let datetime = getObjectFromDatetimeLong(datetimeLong);
    return `${datetime.hour}:${datetime.minute}:${datetime.second} (${datetime.day}/${datetime.month}/${datetime.year})`;
  }

  function setZero(value) {
    if (value >= 0 && value <= 9) {
      return value == 0 ? '00' : `0${value}`;
    }
    return value;
  }

  function getObjectFromDatetimeLong(timestampLong) {
    let timestamp = timestampLong.toString();
    return {
      year: timestamp.substring(0, 4),
      month: timestamp.substring(4, 6),
      day: timestamp.substring(6, 8),
      hour: timestamp.substring(8, 10),
      minute: timestamp.substring(10, 12),
      second: timestamp.substring(12, 14)
    };
  }
}