export function SelectDeviceDirective(ConfigService, DevicesService, NotificationService) {
  'ngInject';

  return {
    templateUrl: 'app/shared/select_device/select_device.view.html',
    link: link,
    restrict: 'E',
    scope: {
      selectedDevice: '='
    }
  };

  function link(scope) {

    scope.devices = null;

    initDevices();

    function initDevices() {
      let profile = ConfigService.getProfile();
      DevicesService.getDevices(profile.userName)
        .then(getDevicesSuccess)
        .catch((error) => {
          NotificationService.errorKey('ERROR_DOWNLOAD_DATA');
          console.log(error);
        });

      function getDevicesSuccess(response) {
        if (response.data) {
          scope.devices = response.data;
          scope.devices.forEach(dev => { 
            let desc = dev.description ? dev.description : '';
            dev.display = `${dev.name} (${dev.imei}) ${desc}`;
          });
        }
      }
    }
  }
}