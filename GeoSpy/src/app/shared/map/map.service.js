export function MapService($document, ConfigConst, ParseHelperService) {
  'ngInject';

  class Map {
    constructor(mapElemId) {
      /* eslint-disable */
      this._google = google;
      /* eslint-enable */
      this._mapElemId = mapElemId;
      this._roadLocationsMarkers = null;
      this._roadPath = null;
      this._locationMarker = null;

      this.initMap();
    }

    get map() {
      return this._map;
    }

    set settings(value) {
      this._settings = value;
    }

    set zoom(value) {
      this._settings.zoom = value;
    }

    initMap() {
      this._mapElem = $document[0].getElementById(this._mapElemId);
      this._setDefaultSettings();
      this._drawMap();
    }

    setCenter(lat, lng) {
      this._settings.center = { lat, lng };
    }

    drawRoadPath(roadPath) {
      this.clearSnappedRoadPath();

      let path = this._getPathFromRoadPoints(roadPath.roadPoints);

      this._roadPath = new this._google.maps.Polyline({
        path: path,
        strokeColor: 'purple',
        strokeWeight: 4
      });

      this._roadPath.setMap(this._map);
      this._zoomToObject(this._roadPath);
    }

    drawRoadLocationsMarkers(roadLocations) {
      this.clearRoadLocationsMarkers();

      let markersData = this._getMarkersDataFromLocations(roadLocations);
      if (markersData) {
        this._roadLocationsMarkers = markersData.map(data => this._getMarker({position: data.position, title: data.timestamp}));
      }

      this._roadLocationsMarkers.forEach(marker => marker.setMap(this._map));

      if (this._roadPath) {
        this._zoomToObject(this._roadPath);
      } else {
        this._zoomToMarker(this._roadLocationsMarkers[0], 15);
      }
    }

    drawLocationMarker(location) {
      this.clearLocationMarker();

      let markerData = this._getMarkerDataFromLocation(location);
      let data = {
        position: markerData.position,
        title: markerData.timestamp
      };

      // let icon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
      // let icon = 'assets/icons/Marker-48-Red.png';
      this._locationMarker = new this._google.maps.Marker({
        position: data.position,
        title: data.title
      });

      // this._locationMarker = this._getMarker(data);
      this._locationMarker.setMap(this._map);
      this._zoomToMarker(this._locationMarker, 17);
    }

    clearLocationMarker() {
      if (this._locationMarker) {
        this._locationMarker.setMap(null);
      }

      this._locationMarker = null;
    }

    clearSnappedRoadPath() {
      if (this._roadPath) {
        this._roadPath.setMap(null);
      }

      this._roadPath = null;
    }

    clearRoadLocationsMarkers() {
      if (this._roadLocationsMarkers) {
        this._roadLocationsMarkers.forEach(marker => marker.setMap(null));
      }

      this._roadLocationsMarkers = null;
    }

    _getMarkersDataFromLocations(locations) {
      let result = null;
      if (locations && locations.length > 0) {
        result = locations.map(loc => this._getMarkerDataFromLocation(loc));
      }
      return result;
    }

    _getMarkerDataFromLocation(location) {
      return {
        position: new this._google.maps.LatLng(parseFloat(location.latitude), parseFloat(location.longitude)),
        timestamp: ParseHelperService.getDateTimeStringDisplay(location.timestamp)
      };
    }

    _setDefaultSettings() {
      this._settings = {
        zoom: 5,
        center: { lat: 50.061670, lng: 19.937367 }
      };
    }

    _drawMap() {
      this._map = new this._google.maps.Map(this._mapElem, this._settings);
    }

    _getMarker({position, title}) {
      return new this._google.maps.Marker({ position, title });
    }

    _getPathFromRoadPoints(roadPoints) {
      let path = [];
      for (let i = 0, length = roadPoints.length; i < length; i++) {
        let latlng = new this._google.maps.LatLng(
          roadPoints[i].latitude,
          roadPoints[i].longitude);
        path.push(latlng);
      }
      return path;
    }

    _zoomToObject(obj) {
      let bounds = new this._google.maps.LatLngBounds();
      let points = obj.getPath().getArray();
      for (let n = 0; n < points.length; n++) {
        bounds.extend(points[n]);
      }
      this._map.fitBounds(bounds);
    }

    _zoomToMarker(marker, zoom) {
      if (marker) {
        this._map.setZoom(zoom);
        this._map.panTo(marker.position);
      }
    }
  }

  return {
    mapBuild: (mapElemId) => new Map(mapElemId)
  };
}