export function DatePickerDirective() {
  'ngInject';

  return {
    templateUrl: 'app/shared/date_picker/date_picker.view.html',
    restrict: 'E',
    link: link,
    scope: {
      date: '='
    }
  };

  function link(scope) {
    scope.date = new Date();
  }
}