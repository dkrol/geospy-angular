export function UsersController($scope, UsersService, NotificationService, ConfigService, TableService, ngDialog) {
  'ngInject';

  const DIALOG_CONFIRM = 'confirm';

  let vm = this;
  vm.scope = $scope;
  vm.userClick = userClick;
  vm.editUserClick = editUserClick;
  vm.isAnyUserSelected = isAnyUserSelected;

  initUsersController();

  function initUsersController() {
    getUsers();
  }

  function getUsers() {
    ConfigService.isLoading = true;

    UsersService.getUsers()
      .then(getUsersSuccess)
      .catch((error) => {
        NotificationService.errorResponse(error);
      })
      .finally(() => { ConfigService.isLoading = false; });

    function getUsersSuccess(response) {
      if (response.data) {
        vm.users = response.data;
        initUsersSettings(vm.users);
        initUsersTable(vm.users);
      }
    }
  }

  function initUsersSettings(users) {
    users.forEach(usr => {
      usr.settings = {
        isEdited: false,
        isSelected: false,
        roleName: usr.role.name
      }
    });
  }

  function userClick(user) {
    let isSelected = user.settings.isSelected;
    deselectAllUsers(vm.users);
    user.settings.isSelected = !isSelected;

    if (user.settings.isSelected) {
      initDevicesTable(user.devices);
    }
  }

  function editUserClick() {
    let editedUser = getSelectedUserForEdit();
    let dialog = ngDialog.open({
      template: 'app/users/modals/user_edit.view.html',
      data: {
        user: editedUser
      }
    });

    dialog.closePromise.then(onEditDialogClose);
    function onEditDialogClose(data) {
      let selectedUser = getSelectedUser();
      if (data.value === DIALOG_CONFIRM && isEditedUserChanged(editedUser, selectedUser)) {
        UsersService.updateUser(editedUser)
          .then(() => {
            initUsersController();
            NotificationService.successKey('UPDATE_SUCCESS');
          })
          .catch(() => { NotificationService.errorKey('UPDATE_FAILED') });
      }
    }
  }

  function isEditedUserChanged(editedUser, selectedUser) {
    return editedUser.userName !== selectedUser.userName ||
      editedUser.roleName !== selectedUser.role.name
  }

  function getSelectedUserForEdit() {
    let selectedUser = getSelectedUser();
    return {
      id: selectedUser.id,
      userName: selectedUser.userName,
      roleName: selectedUser.role.name
    };
  }

  function initUsersTable(users) {
    vm.usersTableParams = TableService.startBuild().withDataset(users).build();
  }

  function initDevicesTable(devices) {
    vm.userDevicesTableParams = TableService.startBuild().withDataset(devices).build();
  }

  function deselectAllUsers(users) {
    users.forEach(usr => usr.settings.isSelected = false);
  }

  function getSelectedUser() {
    let result = null;
    if (vm.users) {
      result = vm.users.find(usr => usr.settings.isSelected);
    }

    return result;
  }

  function isAnyUserSelected() {
    return vm.users && vm.users.find(usr => usr.settings.isSelected) ? true : false;
  }
}