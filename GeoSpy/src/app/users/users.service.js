export function UsersService($http, ConfigService, ConfigConst) {
  'ngInject';

  let headers = ConfigService.getHeaders();

  return {
    getUsers: getUsers,
    updateUser: updateUser
  };

  function getUsers() {
    let url = `${ConfigConst.URLS.USERS}`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }

  function updateUser(user) {
    let url = `${ConfigConst.URLS.USERS}/${user.id}`;
    let data = {
      roleName: user.roleName
    };

    return $http({
      method: 'PUT',
      url: url,
      headers: headers,
      data: data
    });
  }
}