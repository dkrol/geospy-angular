export function SigninService($http, ConfigConst) {
  'ngInject';

  return {
    signin: signin
  };

  function signin(data) {
    let url = `${ConfigConst.URLS.API}/jwt`;
    let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    return $http({
      method: 'POST',
      url: url,
      data: data,
      headers: headers,
      transformRequest: transformRequest
    });
  }

  function transformRequest(obj) {
    let str = [];
    for (let p in obj) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }

    return str.join("&");
  }
}
