export function SigninController($state, SigninService, NotificationService, ConfigService, $window, ModelService) {
  'ngInject';
  let vm = this;

  vm.userData = ModelService.userBuild();
  vm.signinClick = signinClick;

  function signinClick() {
    if (vm.userData.userName && vm.userData.password) {
      ConfigService.isLoading = true;
      SigninService.signin(vm.userData.getSignin())
        .then(signinSuccess)
        .catch((error) => { NotificationService.errorResponse(error) })
        .finally(() => { ConfigService.isLoading = false; });
    } else {
      NotificationService.errorKey('INVALID_CREDENTIALS');
    }

    function signinSuccess(response) {
      if (response.data) {
        vm.userData.token = response.data.access_token;
        vm.userData.isLogged = true;
        ConfigService.setProfile(vm.userData.getSigned());
        $window.location.href = '/';
      }
    }
  }
}
