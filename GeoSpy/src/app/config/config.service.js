export class ConfigService {
  constructor(ConfigConst, LocalStorageService, $rootScope, jwtHelper) {
    'ngInject';

    this._configConst = ConfigConst;
    this._localStorageService = LocalStorageService;
    this._jwtHelper = jwtHelper;
    this._rootScope = $rootScope;
  }

  get isLoading() {
    return this._rootScope.isLoading;
  }

  set isLoading(value) {
    this._rootScope.isLoading = value;
  }

  get headers() {
    let profile = this.getProfile();
    return {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${profile.token}`
    };
  }

  setProfile(currentUser) {
    this._localStorageService.setItem(this._configConst.CURRENT.USER, currentUser);
  }

  getProfile() {
    return this._localStorageService.getItem(this._configConst.CURRENT.USER);
  }

  getJwtDecode() {
    let profile = this.getProfile();
    if (profile) {
      return this._jwtHelper.decodeToken(profile.token);
    }
    return null;
  }

  clearProfile() {
    this._localStorageService.setItem(this._configConst.CURRENT.USER, '');
  }

  setLanguage(lang) {
    this._localStorageService.setItem(this._configConst.CURRENT.LANG, lang);
  }

  getLanguage() {
    return this._localStorageService.getItem(this._configConst.CURRENT.LANG);
  }

  getHeaders() {
    let profile = this.getProfile();
    return {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${profile.token}`
    };
  }
}