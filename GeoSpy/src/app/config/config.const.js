
// const api = 'http://localhost:5001/api';
const api = 'http://dkrol.ddns.net:5002/api';

export const ConfigConst = {
  URLS: {
    API: api,
    LOCATIONS: `${api}/locations`,
    DEVICES: `${api}/devices`,
    ROADS: `${api}/roads`,
    USERS: `${api}/users`
  },
  GOOGLE: {
    SNAP_TO_ROAD: 'https://roads.googleapis.com/v1/snapToRoads',
    KEY: 'AIzaSyAJqThp7W9FotiFLJDUPQPYaiWu9ZSuv2s'
  },
  CURRENT: {
    USER: 'currentUser',
    LANG: 'language'
  }
};