import { OfflineController } from './offline.controller';

export function OfflineDirective() {
  'ngInject';

  return {
    templateUrl: 'app/offline/offline.view.html',
    controller: OfflineController,
    controllerAs: 'offlineCtrl',
    restrict: 'E',
    scope: {}
  };
}
