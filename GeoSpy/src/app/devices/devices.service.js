export function DevicesService($http, ConfigService, ConfigConst) {
  'ngInject';

  let headers = ConfigService.getHeaders();
  return {
    getDevices: getDevices,
    updateDevice: updateDevice
  };

  function getDevices(userName) {
    let url = `${ConfigConst.URLS.DEVICES}/user/${userName}`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }

  function updateDevice(device) {
    let url = `${ConfigConst.URLS.DEVICES}/${device.id}`;
    let data = { description: device.description };

    return $http({
      method: 'PUT',
      url: url,
      headers: headers,
      data: data
    });
  }
}
