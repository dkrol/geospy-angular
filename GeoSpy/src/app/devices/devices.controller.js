export function DevicesController(TableService, DevicesService, NotificationService, ConfigService) {
  'ngInject';
  let vm = this;

  vm.devices = null;
  vm.devicesTableParams = null;

  vm.editedSave = editedSave;
  vm.editedCancel = editedCancel;
  vm.deviceClick = deviceClick;
  vm.isAnyDeviceSelected = isAnyDeviceSelected;
  vm.editDeviceClick = editDeviceClick;
  vm.removeDeviceClick = removeDeviceClick;

  initDevicesController();

  function initDevicesController() {
    getDevices();
  }

  function editedSave(device) {
    ConfigService.isLoading = true;
    device.settings.isEdited = false;
    device.description = device.settings.description;

    DevicesService.updateDevice(device)
      .then(() => { NotificationService.successKey('UPDATE_SUCCESS'); })
      .catch((error) => {
        NotificationService.errorKey('UPDATE_FAILED');
        console.log(error);
      })
      .finally(() => { ConfigService.isLoading = false; });
  }

  function editedCancel(device) {
    device.settings.description = device.description;
    device.settings.isEdited = false;
  }

  function getDevices() {
    let user = ConfigService.getProfile();
    ConfigService.isLoading = true;

    DevicesService.getDevices(user.userName)
      .then(getDevicesSuccess)
      .catch((error) => {
        NotificationService.errorResponse(error);
      })
      .finally(() => { ConfigService.isLoading = false; });

    function getDevicesSuccess(response) {
      if (response.data) {
        vm.devices = response.data;
        initDevicesSettings(vm.devices);
        initDevicesTable(vm.devices);
      }
    }
  }

  function deviceClick(device) {
    if (device && !device.settings.isEdited) {
      let prevDeviceSelected = device.settings.isSelected;
      vm.devices.forEach(dev => dev.settings.isSelected = false);
      device.settings.isSelected = !prevDeviceSelected;
    }
  }

  function initDevicesSettings(devices) {
    devices.forEach(dev => {
      dev.settings = {
        isEdited: false,
        description: dev.description,
        isSelected: false
      }
    });
  }

  function initDevicesTable(devices) {
    vm.devicesTableParams = TableService.startBuild().withDataset(devices).build();
  }

  function editDeviceClick() {
    let selectedDevice = getSelectedDevice();
    selectedDevice.settings.isEdited = true;
    selectedDevice.settings.isSelected = false;
  }

  function removeDeviceClick() {
    // TODO: Implement remove devices
    // let selectedDevice = getSelectedDevice();
  }

  function getSelectedDevice() {
    let result = null;
    if (vm.devices) {
      result =  vm.devices.find(dev => dev.settings.isSelected)
    }
    return result;
  }

  function isAnyDeviceSelected() {
    return vm.devices && vm.devices.find(dev => dev.settings.isSelected) ? true : false;
  }
}
