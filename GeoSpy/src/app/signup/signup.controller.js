export function SignupController($state, SignupService, NotificationService, ModelService, ConfigService) {
  'ngInject';
  let vm = this;

  vm.userData = ModelService.userBuild();
  vm.signupClick = signupClick;

  function signupClick() {
    if (isValid(vm.userData.getSignup())) {
      ConfigService.isLoading = true;
      SignupService.addUser(vm.userData.getSignup())
        .then(addUserSuccess)
        .catch((error) => { NotificationService.errorResponse(error); })
        .finally(() => { ConfigService.isLoading = false; });
    }

    function addUserSuccess() {
      vm.userData = ModelService.userBuild();
      NotificationService.successKey('SINGUP_SUCCESS');
    }
  }

  function isValid(userData) {
    if (userData.userName && userData.password && userData.confirmPassword) {
      return true;
    }
  }
}