export function SignupService($http, ConfigConst) {
  'ngInject';
  
  let headers = { 'Content-Type': 'application/json' };

  return {
    addUser: addUser
  };

  function addUser(data) {
    let url = `${ConfigConst.URLS.USERS}`;
    return $http({
      method: 'POST',
      url: url,
      data: data,
      headers: headers
    });
  }
}