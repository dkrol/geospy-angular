export function TrackingService(ConfigService, ConfigConst, $http) {
  'ngInject';

  let headers = ConfigService.getHeaders();

  return {
    getLastLocationByDeviceId: getLastLocationByDeviceId
  };

  function getLastLocationByDeviceId(deviceId) {
    let url = `${ConfigConst.URLS.LOCATIONS}/device/${deviceId}/last`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }

}