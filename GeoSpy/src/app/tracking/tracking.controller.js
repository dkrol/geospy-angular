export function TrackingController($scope, TrackingService, MapService, $interval, ConfigService, NotificationService, TableService, ParseHelperService) {
  'ngInject';

  const TRACKING_MAP_ID = 'trackingMap';
  const INTERVAL_TIME = 2000;

  let vm = this;
  vm.map = null;
  vm.isTracking = false;
  vm.trackingTimer = null;
  vm.lastTrackingLocation = null;
  vm.trackingLocations = null;

  vm.startTracking = startTracking;
  vm.stopTracking = stopTracking;

  $scope.$watch(() => vm.filter.selectedDevice, stopTracking);
  $scope.$watch(() => vm.filter.isShowTrackingRoad, showTrackingRoadChanged);

  function showTrackingRoadChanged() {
    drawRoadLocationsMarker()
  }

  initTrackingController();

  function initTrackingController() {
    vm.filter = { isShowTrackingRoad: false };
    initTracking();
  }

  function refreshTracking() {
    let deviceId = vm.filter.selectedDevice.id;

    TrackingService.getLastLocationByDeviceId(deviceId)
      .then(getLastLocationByDeviceIdSuccess)
      .catch(getLastLocationByDeviceIdFailed);

    function getLastLocationByDeviceIdSuccess(response) {
      if (response.data) {
        let location = response.data;

        if (isNewLocation(location)) {
          drawRoadLocationsMarker();

          location.settings = { timestamp: ParseHelperService.getDateTimeStringDisplay(location.timestamp) };
          vm.lastTrackingLocation = location;
          vm.trackingLocations.push(location);
          vm.map.drawLocationMarker(vm.lastTrackingLocation);
          initTrackingTable(vm.trackingLocations);
        }
      } else {
        NotificationService.warningKey('NO_INFO_LAST_LOCATION');
      }
    }

    function getLastLocationByDeviceIdFailed(error) {
      console.log(error);
    }
  }

  function drawRoadLocationsMarker() {
    if (vm.filter.isShowTrackingRoad && vm.trackingLocations && vm.trackingLocations.length > 0) {
      vm.map.drawRoadLocationsMarkers(vm.trackingLocations);
    } else {
      vm.map.clearRoadLocationsMarkers();
    }
  }

  function initTrackingTable(tracking) {
    vm.trackingTableParams = TableService.startBuild().withDataset(tracking).build();
  }

  function isNewLocation(location) {
    return !vm.lastTrackingLocation || (vm.lastTrackingLocation.id !== location.id);
  }

  function isFilterValid(filterData) {
    return filterData.selectedDevice && filterData.selectedDevice !== ''
  }

  function initTracking() {
    vm.lastTrackingLocation = null;
    vm.trackingLocations = [];
    vm.map = MapService.mapBuild(TRACKING_MAP_ID);
  }

  function startTracking() {
    if (isFilterValid(vm.filter)) {
      initTracking();
      vm.isTracking = true;
      vm.trackingTimer = $interval(refreshTracking, INTERVAL_TIME);
      refreshTracking();
    } else {
      NotificationService.errorKey('FILTER_EMPTY_FIELDS');
    }
  }

  function stopTracking() {
    vm.isTracking = false;
    if (angular.isDefined(vm.trackingTimer)) {
      $interval.cancel(vm.trackingTimer);
    }
  }
}