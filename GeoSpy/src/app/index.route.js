export function routerConfig($stateProvider, $urlRouterProvider, PermissionConst) {
  'ngInject';
  $stateProvider
    .state('signin', {
      url: '/',
      views: {
        'offlineView': {
          templateUrl: 'app/signin/signin.view.html',
          controller: 'SigninController',
          controllerAs: 'signinCtrl',
          data: {
            title: 'SIGNIN'
          }
        }
      },
      data: {
        permissions: {
          except: [PermissionConst.ROLE_NAME.AUTHORIZED],
          redirectTo: 'dashboard'
        }
      }
    })
    .state('signup', {
      url: '/signup',
      views: {
        'offlineView': {
          templateUrl: 'app/signup/signup.view.html',
          controller: 'SignupController',
          controllerAs: 'signupCtrl',
          data: {
            title: 'SIGNUP'
          }
        }
      },
      data: {
        permissions: {
          except: [PermissionConst.ROLE_NAME.AUTHORIZED],
          redirectTo: 'dashboard'
        }
      }
    })
    .state('dashboard', {
        url: '/dashboard',
        views: {
          'onlineView': {
            templateUrl: 'app/dashboard/dashboard.view.html',
            controller: 'DashboardController',
            controllerAs: 'dashCtrl',
            data: {
              title: 'DASHBOARD'
            }
          }
        },
        data: {
          permissions: {
            only: [PermissionConst.ROLE_NAME.AUTHORIZED],
            redirectTo: 'signin'
          }
        }
      })
      .state('devices', {
        url: '/devices',
        views: {
          'onlineView': {
            templateUrl: 'app/devices/devices.view.html',
            controller: 'DevicesController',
            controllerAs: 'devCtrl',
            data: {
              title: 'DEVICES'
            }
          }
        },
        data: {
          permissions: {
            only: [PermissionConst.ROLE_NAME.AUTHORIZED],
            redirectTo: 'signin'
          }
        }
      })
      .state('locations', {
        url: '/locations',
        views: {
          'onlineView': {
            templateUrl: 'app/locations/locations.view.html',
            controller: 'LocationsController',
            controllerAs: 'locCtrl',
            data: {
              title: 'LOCATIONS'
            }
          }
        },
        data: {
          permissions: {
            only: [PermissionConst.ROLE_NAME.AUTHORIZED],
            redirectTo: 'signin'
          }
        }
      })
      .state('tracking', {
        url: '/tracking',
        views: {
          'onlineView': {
            templateUrl: 'app/tracking/tracking.view.html',
            controller: 'TrackingController',
            controllerAs: 'trackingCtrl',
            data: {
              title: 'TRACKING'
            }
          }
        },
        data: {
          permissions: {
            only: [PermissionConst.ROLE_NAME.AUTHORIZED],
            redirectTo: 'signin'
          }
        }
      })
      .state('users', {
        url: '/users',
        views: {
          'onlineView': {
            templateUrl: 'app/users/users.view.html',
            controller: 'UsersController',
            controllerAs: 'usrCtrl',
            data: {
              title: 'USERS'
            }
          }
        },
        data: {
          permissions: {
            only: [PermissionConst.ROLE_NAME.ADMIN],
            redirectTo: 'signin'
          }
        }
      });

  $urlRouterProvider.otherwise('/');
}
