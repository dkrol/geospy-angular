export function runBlock($log, $rootScope, ConfigService, PermissionService, PermissionConst) {
  'ngInject';

  $rootScope.$on('$destroy', onDestroy);
  $rootScope.roleName = PermissionConst.ROLE_NAME;
  $rootScope.isLoading = false;
  $rootScope.googleMapApiKey = 'AIzaSyAJqThp7W9FotiFLJDUPQPYaiWu9ZSuv2s';
  $rootScope.googleMapApi = `https://maps.googleapis.com/maps/api/js?key=${$rootScope.googleMapApiKey}`;
  let stateChangeStartVar = $rootScope.$on('$stateChangeStart', stateChangeStart);
  // let stateChangePermissionStartVar = $rootScope.$on('$stateChangePermissionStart', stateChangePermissionStart);
  // let stateChangePermissionAcceptedVar = $rootScope.$on('$stateChangePermissionAccepted', stateChangePermissionAccepted);
  // let stateChangePermissionDeniedVar = $rootScope.$on('$stateChangePermissionDenied', stateChangePermissionDenied);

  definePermission();

  function definePermission() {
    PermissionService.definePermission();
  }

  function stateChangeStart(event, toState) {
    if (toState.views && toState.views.offlineView) {
      $rootScope.title = toState.views.offlineView.data.title;
    } else if (toState.views && toState.views.onlineView) {
      $rootScope.title = toState.views.onlineView.data.title;
    }
  }

  // function stateChangePermissionStart(event, toState, toParams, options) {
  //   console.log('STATE CHANGE PERMISSION START');
  // }

  // function stateChangePermissionAccepted(event, toState, toParams, options) {
  //   console.log('STATE CHANGE PERMISSION ACCEPTED');
  // }

  // function stateChangePermissionDenied(event, toState, toParams, options) {
  //   console.log('STATE CHANGE PERMISSION DENIED');
  // }

  function onDestroy() {
    stateChangeStartVar();
    // stateChangePermissionStartVar();
    // stateChangePermissionAcceptedVar();
    // stateChangePermissionDeniedVar();
  }
}
