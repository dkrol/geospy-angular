import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { LocalStorageService } from './shared/local_storage/local_storage.service';
import { PermissionConst } from './shared/permission/permission.const';
import { PermissionService } from './shared/permission/permission.service';
import { NotificationService } from './shared/notification/notification.service';
import { ConfigConst } from './config/config.const';
import { OnlineConst } from './online/online.const';
import { ConfigService } from './config/config.service';
import { OfflineDirective } from './offline/offline.directive';
import { OnlineDirective } from './online/online.directive';
import { SigninController } from './signin/signin.controller';
import { SignupController } from './signup/signup.controller';
import { SigninService } from './signin/signin.service';
import { SignupService } from './signup/signup.service';
import { DashboardController } from './dashboard/dashboard.controller';
import { DevicesController } from './devices/devices.controller';
import { DevicesService } from './devices/devices.service';
import { LanguageController } from './shared/language/language.controller';
import { LanguageDirective } from './shared/language/language.directive';
import { ModelService } from './shared/model/model.service';
import { UserService } from './shared/model/user.service';
import { ParseHelperService } from './shared/helpers/parse_helper.service';
import { LocationsController } from './locations/locations.controller';
import { LocationsService } from './locations/locations.service';
import { SelectDeviceDirective } from './shared/select_device/select_device.directive';
import { MapService } from './shared/map/map.service';
import { DatePickerDirective } from './shared/date_picker/date_picker.directive';
import { TrackingController } from './tracking/tracking.controller';
import { TrackingService } from './tracking/tracking.service';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { TableService } from './shared/table/table.service';

angular.module('geoSpy', [
  'ngAnimate',
  'ngCookies',
  'ngMessages',
  'ngMaterial',
  'ngAria',
  'ngLocale',
  'ui.router',
  'ui.bootstrap',
  'toastr',
  'permission',
  'permission.ui',
  'ui-notification',
  'pascalprecht.translate',
  'ngTable',
  'angular-jwt',
  'ngDialog'])

  .constant('PermissionConst', PermissionConst)
  .constant('ConfigConst', ConfigConst)
  .constant('OnlineConst', OnlineConst)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service('ConfigService', ConfigService)
  .service('ModelService', ModelService)
  .service('UserService', UserService)
  .service('LocalStorageService', LocalStorageService)
  .service('PermissionService', PermissionService)
  .service('ParseHelperService', ParseHelperService)
  .service('NotificationService', NotificationService)
  .service('SigninService', SigninService)
  .service('SignupService', SignupService)
  .service('DevicesService', DevicesService)
  .service('LocationsService', LocationsService)
  .service('MapService', MapService)
  .service('TrackingService', TrackingService)
  .service('UsersService', UsersService)
  .service('TableService', TableService)
  .controller('SigninController', SigninController)
  .controller('SignupController', SignupController)
  .controller('DashboardController', DashboardController)
  .controller('DevicesController', DevicesController)
  .controller('LanguageController', LanguageController)
  .controller('LocationsController', LocationsController)
  .controller('TrackingController', TrackingController)
  .controller('UsersController', UsersController)
  .directive('offline', OfflineDirective)
  .directive('online', OnlineDirective)
  .directive('language', LanguageDirective)
  .directive('selectDevice', SelectDeviceDirective)
  .directive('datePicker', DatePickerDirective)
