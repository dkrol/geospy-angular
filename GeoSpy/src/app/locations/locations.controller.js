export function LocationsController($scope, $document, ConfigService, LocationsService, ParseHelperService, NotificationService, MapService, TableService) {
  'ngInject';
  const LOCATIONS_MAP_ID = 'locationsMap';
  let vm = this;

  vm.filter = null;
  vm.map = null;
  vm.road = null;
  vm.snappedLocations = null;
  vm.roadTableParams = null;

  vm.searchClick = searchClick;
  vm.roadPathClick = roadPathClick;

  $scope.$watch(() => vm.filter.isShowRoadLocations, updateDrawSelectedRoad);
  $scope.$watch(() => vm.filter.isShowRoadPath, updateDrawSelectedRoad);

  initLocationsController();

  function initLocationsController() {
    vm.filter = {
      isShowRoadPath: true,
      isShowRoadLocations: false
    };

    initLocationMap();
  }

  function initLocationMap() {
    vm.map = MapService.mapBuild(LOCATIONS_MAP_ID);
  }

  function searchClick() {
    if (isFilterValid(vm.filter)) {
      ConfigService.isLoading = true;
      let data = prepareData(vm.filter);
      getRoadForDevice(data);
    } else {
      NotificationService.errorKey('FILTER_EMPTY_FIELDS');
    }
  }

  function isFilterValid(filterData) {
    return filterData.selectedDevice 
      && filterData.selectedDevice !== ''
      && filterData.date
  }

  function prepareData(filterData) {
    return {
      deviceId: filterData.selectedDevice.id,
      date: ParseHelperService.getStringFromDate(filterData.date)
    };
  }

  function getRoadForDevice(data) {
    LocationsService.getRoadByDeviceIdAndDate(data)
      .then(getRoadByDeviceIdAndDateSuccess)
      .catch(getRoadsByDeviceIdAndDateFailed)
      .finally(() => { ConfigService.isLoading = false; });

    function getRoadByDeviceIdAndDateSuccess(response) {
      if (response.data && response.data.roadPaths) {
        vm.road = response.data;
        prepareRoadPaths(vm.road.roadPaths);
        initRoadPathTable(vm.road.roadPaths);
      } else {
        NotificationService.warningKey('NO_DATA');
      }
    }

    function getRoadsByDeviceIdAndDateFailed(error) {
      NotificationService.errorKey('ERROR_DOWNLOAD_DATA');
      console.log(error);
    }
  }

  function prepareRoadPaths(roadPaths) {
    roadPaths.forEach(rPath => {
      rPath.settings = {
        isSelected: false,
        dateFromString: ParseHelperService.getDateTimeStringDisplay(rPath.dateFrom),
        dateToString: ParseHelperService.getDateTimeStringDisplay(rPath.dateTo)
      };
    });
  }

  function initRoadPathTable(roadPaths) {
    vm.roadTableParams = TableService.startBuild().withDataset(roadPaths).build();
  }

  function roadPathClick(roadPath) {
    vm.road.roadPaths.forEach(rPath => rPath.settings.isSelected = false);
    roadPath.settings.isSelected = true;

    let data = getSelectedRoadPathWithRoadLocations();
    drawRoadPathAndRoadLocations(data);
  }

  function drawRoadPathAndRoadLocations(data) {
    if (vm.filter.isShowRoadPath) {
      vm.map.drawRoadPath(data.roadPath);
    } else {
      vm.map.clearSnappedRoadPath();
    }

    if (vm.filter.isShowRoadLocations) {
      vm.map.drawRoadLocationsMarkers(data.roadLocations);
    } else {
      vm.map.clearRoadLocationsMarkers();
    }
  }

  function updateDrawSelectedRoad() {
    let data = getSelectedRoadPathWithRoadLocations();
    if (data) {
      drawRoadPathAndRoadLocations(data);
    }
  }

  function getSelectedRoadPathWithRoadLocations() {
    let result = null;
    if (vm.road) {
      let roadPath = vm.road.roadPaths.find(roadPath => roadPath.settings.isSelected);

      if (roadPath) {
        let index = vm.road.roadPaths.indexOf(roadPath);
        let roadLocations = vm.road.roadLocations[index];
        result = { roadPath, roadLocations };
      }
    }

    return result;
  }
}
