export function LocationsService($http, ConfigService, ConfigConst) {
  'ngInject';

  let headers = ConfigService.getHeaders();

  return {
    getLastDeviceLocationByDeviceId: getLastDeviceLocationByDeviceId,
    getLocationsByDeviceId: getLocationsByDeviceId,
    getLocationsByDeviceIdAndTimestampRange: getLocationsByDeviceIdAndTimestampRange,
    getRoadByDeviceIdAndDate: getRoadByDeviceIdAndDate
  };

  function getLastDeviceLocationByDeviceId(deviceId) {
    let url = `${ConfigConst.URLS.LOCATIONS}/device/${deviceId}/last`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }

  function getLocationsByDeviceId(deviceId) {
    let url = `${ConfigConst.URLS.LOCATIONS}/device/${deviceId}`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }

  function getLocationsByDeviceIdAndTimestampRange(data) {
    let url = `${ConfigConst.URLS.LOCATIONS}/device/${data.deviceId}/from/${data.datetimeRange.from}/to/${data.datetimeRange.to}`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }

  function getRoadByDeviceIdAndDate(data) {
    let url = `${ConfigConst.URLS.ROADS}/device/${data.deviceId}/date/${data.date}`;
    return $http({
      method: 'GET',
      url: url,
      headers: headers
    });
  }
}
