export function DashboardController(OnlineConst, ConfigService) {
  'ngInject';
  let vm = this;

  vm.userProfile = ConfigService.getProfile();
  vm.role = ConfigService.getJwtDecode().role;

  initDashboardController();

  function initDashboardController() {
    vm.items = OnlineConst.MENU_ITEMS;
  }
}