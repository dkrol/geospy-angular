export function config($logProvider, toastrConfig, NotificationProvider, $translateProvider, $mdDateLocaleProvider) {
  'ngInject';

  // Enable log
  $logProvider.debugEnabled(true);

  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;

  // Set callendar options
  $mdDateLocaleProvider.months = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];
  $mdDateLocaleProvider.shortMonths = ['Sty', 'Lut', 'Marz', 'Kwie', 'Maj', 'Czer', 'Lip', 'Sier', 'Wrze', 'Paź', 'List', 'Grudź'];
  $mdDateLocaleProvider.days = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];
  $mdDateLocaleProvider.shortDays = ['N', 'P', 'W', 'Ś', 'C', 'P', 'S'];

  $mdDateLocaleProvider.formatDate = function (date) {
    let result = '';
    if (date) {
      let day = date.getDate();
      let month = date.getMonth();
      let year = date.getFullYear();
      result = `${day} ${$mdDateLocaleProvider.months[month]} ${year}`;
    }
    return result;
  };

  // Notification configuration
  NotificationProvider.setOptions({
    delay: 6000,
    startTop: 20,
    startRight: 20,
    verticalSpacing: 20,
    horizontalSpacing: 10,
    positionX: 'right',
    positionY: 'bottom'
  });

  // Translation configuration
  const LANGUAGE = 'language';
  const LOCALE = {
    PL: { SHORT: 'pl', FULL: 'pl-PL' },
    EN: { SHORT: 'en', FULL: 'en-US' }
  }

  $translateProvider.useStaticFilesLoader({
    prefix: 'assets/i18n/messages_',
    suffix: '.json'
  });

  $translateProvider.preferredLanguage(getDefaultLanguage());
  $translateProvider.useSanitizeValueStrategy(null);

  function getDefaultLanguage() {
    let lang = angular.fromJson(window.localStorage.getItem(LANGUAGE));

    if (lang === null) {
      let langNavigator = window.navigator.userLanguage || window.navigator.language;
      lang = getLanguage(langNavigator);
      window.localStorage.setItem(LANGUAGE, angular.toJson(lang));
    }

    return lang;

    function getLanguage(lang) {
      switch (lang) {
        case LOCALE.PL.SHORT:
        case LOCALE.PL.FULL:
          return LOCALE.PL.SHORT;
        case LOCALE.EN.SHORT:
        case LOCALE.EN.FULL:
          return LOCALE.EN.SHORT;
        default:
          return LOCALE.EN.SHORT;
      }
    }
  }
}
