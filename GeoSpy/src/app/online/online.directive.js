import { OnlineController } from './online.controller'

export function OnlineDirective() {
  'ngInject';

  return {
    templateUrl: 'app/online/online.view.html',
    controller: OnlineController,
    controllerAs: 'onlineCtrl',
    restrict: 'E',
    scope: {}
  };
}