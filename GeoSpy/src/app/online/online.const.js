export const OnlineConst = {
  MENU_ITEMS: [{
    header: 'DASHBOARD',
    uisref: 'dashboard',
    iconClass: 'fa fa-home',
    role: 'AUTHORIZED'
  },
  {
    header: 'DEVICES',
    uisref: 'devices',
    iconClass: 'fa fa-tablet',
    role: 'AUTHORIZED'
  },
  {
    header: 'LOCATIONS',
    uisref: 'locations',
    iconClass: 'fa fa-map',
    role: 'AUTHORIZED'
  },
  {
    header: 'TRACKING',
    uisref: 'tracking',
    iconClass: 'fa fa-map-marker',
    role: 'AUTHORIZED'
  },
  {
    header: 'USERS',
    uisref: 'users',
    iconClass: 'fa fa-user',
    role: 'ADMIN'
  }]
};