export class OnlineController {
  constructor($window, $document, ConfigService, $translate, NotificationService, OnlineConst) {
    'ngInject';

    this._window = $window;
    this._document = $document;
    this._configService = ConfigService;
    this._translate = $translate;
    this._notificationService = NotificationService;
    this._onlineConst = OnlineConst;

    this.isMenuOpened = false;
    this._initOnlineController();
  }

  _initOnlineController() {
    this._closeNav();
    this.menuItems = this._onlineConst.MENU_ITEMS;
  }

  menuAction() {
    if (this.isMenuOpened) {
      this._closeNav();
    } else {
      this._openNav();
    }
  }

  logoutClick() {
    this._configService.clearProfile();
    this._window.location.href = '/';
  }

  _openNav() {
    this._updateSidenav(250);
    this.isMenuOpened = true;
  }

  _closeNav() {
    this._updateSidenav(0);
    this.isMenuOpened = false;
  }

  _updateSidenav(value) {
    this._document[0].getElementById('onlineSidenav').style.width = `${value}px`;
    this._document[0].getElementById('onlineBody').style.marginLeft = `${value}px`;
  }
}